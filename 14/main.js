function replaceChar(word){
    //replace all occurences of the first character of the string with an underscore
    if(typeof(word) !== 'string'){
        console.log('argument :' + word + 'must be string');
        return -1;
    }
    var pattern = new RegExp(word[0],"g");
    word = word.replace(pattern,'_');
    return word;
}


function replaceCharFast(word){
    //replace all occurences of the first character of the string with an underscore excep the first one
    if(typeof(word) !== 'string'){
        console.log('argument :' + word + 'must be string');
        return -1;
    }
    var result;
    var first_char = word.charAt(0);
    var arr = word.split(first_char);
    arr[1]=first_char + arr[1];
    
    result = arr.join('_').slice(1);
    return result;
}
console.log(replaceCharFast('helloWorld'));
console.log(replaceCharFast('oxymoron'));