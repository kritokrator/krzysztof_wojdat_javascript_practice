function shuffle(arr){
    var rand, temp;
    for(var i = arr.length; i > 0;i--){
    rand = (Math.random() * i) | 0;
    temp = arr[i-1];
    arr[i-1] = arr[rand];
    arr[rand] = temp;
}
  return arr;                                  
};

function markAnswer(selected,version){
    //helper function for marking the selected answer
    //switches the background of the selected div
    if(version === 'mark'){
    if(selected.hasClass('right')){
            selected.css({
            "background-image" : "url('img/answer-right-marked.png')"
        });    
        }else{
            selected.css({
            "background-image" : "url('img/answer-left-marked.png')"
        });
        }
    }
    else if(version === 'wrong'){
        if(selected.hasClass('right')){
            selected.css({
            "background-image" : "url('img/answer-right-wrong.png')"
        });    
        }else{
            selected.css({
            "background-image" : "url('img/answer-left-wrong.png')"
        });
        }
    } else if(version === 'correct'){
        if(selected.hasClass('right')){
            selected.css({
            "background-image" : "url('img/answer-right-correct.png')"
        });    
        }else{
            selected.css({
            "background-image" : "url('img/answer-left-correct.png')"
        });
        }
    }
    
}

function fillQuestion(question){
    //a helper functioin for filling the question and answers
    var counter = 0;
    
    //ranomize the anaswers
    question["answers"] = shuffle(question["answers"]);
    
    $('#question_container').html(question["question"]);
    $('.answer').each(function(){
            $(this).html(question["answers"][counter]);
            $(this).removeAttr("disabled").on('click');
            counter++;
    });
}


function getRandomIndex(questions,usedIndexes){
    var elem = questions.length;
    var tempArr = [];
    var random;
    
    for(var i =0;i<elem;i++){
        if(usedIndexes.indexOf(i) == -1){
            tempArr.push(i);
        }
    }
    
    random = (Math.random() * tempArr.length) | 0;
    usedIndexes.push(tempArr[random]);
    return tempArr[random];
}


$(document).ready(function(){
    var questions = [
        {
            "question" : "ile czasu potrzeba aby przejść z punktu A do punktu B",
            "answers" : ["5 godzin","2 lata","3 minuty","wiem ale nie powiem"],
            "correctAnswer" : "5 godzin",
            "tip": "3 minuty"
            
        },
        {
            "question" : "Dlaczego nie ma prądu",
            "answers" : ["bo agregat padł","bo Bóg Cię nienawidzi","bo nie zapłacono rachunku","wiem ale nie powiem"],
            "correctAnswer" : "bo agregat padł",
            "tip": "3 minuty"
            
        },
        {
            "question" : "Dlaczego ogórek nie śpiewa",
            "answers" : ["bo jest konserwowy","bo go zjedzono","bo nie może","bo ma treme"],
            "correctAnswer" : "bo nie może",
            "tip": "bo jest konserwowy"
            
        }
    ];
    
    var usedIndexes = [];
    
    var currentQuestion;
    
    function getRandQuestion(){
        //helper function returning a random question from the questions array
        return questions[getRandomIndex(questions,usedIndexes)];
    }
    
function endGame(win,answer){
    if(win){
        console.log('Win')        
    }
    else{
        console.log('lose');
        markAnswer(answer,'wrong');
        $('.answer').each(function(index,value){
            if($(this).html() == currentQuestion["correctAnswer"]){
                markAnswer($(this),'correct');
            }
        })
        
    }
    
    
}

    
    currentQuestion = getRandQuestion();
    
    fillQuestion(currentQuestion);
    
    $('.answer').click(function(){
        //markAnswer($(this));
        if($(this).html() === currentQuestion["correctAnswer"]){
            console.log("Brawe");
            currentQuestion = getRandQuestion();
            fillQuestion(currentQuestion);
            console.log(usedIndexes);
        }
        else{
            console.log("WRONG");
            endGame(false,$(this));
        }
    });
    
    function getHelp(button){
        alert(currentQuestion["tip"]);
        button.attr('disabled','disabled');
        
    }
    
    function half(button){
        button.attr('disabled','disabled');
        var tmpArr = [];
        $('.answer').each(function(i,e){
            if($(this).html() != currentQuestion["correctAnswer"])
                tmpArr.push($(this)); 
        });
        var answ_1 = tmpArr.pop();
        answ_1.css("color","#000000");
        answ_1.attr("disabled","disabled").off('click');
        var answ_2 = tmpArr.pop();
        answ_2.attr("disabled","disabled").off('click');
        answ_2.css("color","#000000");
        
        
        console.log(currentQuestion["correctAnswer"]);
    }
    
    function audienceHelp(){
        var tempValues = [], sum =0, tempPercentage = [];
        str = "";
        for(var i = 0; i <4; i++){
            tempValues[i] = (Math.random() * 50) | 0;
            sum += tempValues[i];
        }
        for(var i = 0; i <4; i++){
            tempPercentage[i] = ((tempValues[i]/sum) * 100) | 0;
        }
        console.log(tempPercentage);
        tempPercentage = tempPercentage.sort(function(a,b){
            //need to provide my own comparator
            return a - b;
        });
        
        console.log(tempPercentage);
        
        $('.answer').each(function(i,e){
            console.log(e.dataset.letter, (e.innerHTML == currentQuestion['correctAnswer']));
            if(e.innerHTML == currentQuestion['correctAnswer']){
                str += e.dataset.letter + ":" + tempPercentage.pop() + "\n";
            }else{
                str += e.dataset.letter + ":" + tempPercentage.shift() + "\n";
            }
        });
        alert(str);
    }
    
    $('.fifty-fifty').click(function(){
        half($(this));
    });
    
    $('.friend-help').click(function(){
        getHelp($(this));
    });
    
    $('.get-help-from-audience').click(function(){
        audienceHelp();
    });
});