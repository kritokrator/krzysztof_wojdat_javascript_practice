function ears(rabbits){
    //return the number of ears rabbits have, knowing that the odd rabbits have only one ear
    if(rabbits <= 0){
        console.log("rabbits cannot be less than 0")
        return -1;
    }
    if(typeof(rabbits) !== 'number'){
        console.log("rabbits has to be a number")
        return -1;
    }
    var oddRabbits = 0;
    var evenRabbits = rabbits/2;
    var ears = 0;
    if(rabbits % 2 == 1){
        oddRabbits = ((rabbits - 1)/2) + 1;
        evenRabbits = (((rabbits -1)- 2)/2) + 1;
    }else{
        oddRabbits = (((rabbits -1)- 1)/2) + 1;
        evenRabbits =((rabbits - 2)/2) + 1;
    }
    ears = oddRabbits + (evenRabbits *2);
    return ears;
}

function ears2(n){
    if(n%2 !=0){
        n--;
        return 3/2 * n +1;
    } else{
        return 3/2 *n;
    }
}
console.log(ears(1));
console.log(ears(2));
console.log(ears(3));
console.log(ears(4));
console.log(ears("uszy"));
console.log(ears(-1));
console.log(ears(10000000000000));