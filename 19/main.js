function printDivisible(number){
    //print all numbers from 0 to argument divisible by 3 and 4
     if(typeof(number) !== 'number'){
        console.log('argument is not a number');
        return -1;
    }
    if(number < 0){
        console.log('argument is less than 0');
        return -1;
    }
    for(var i = 0; i <= number;i++){
        if(i % 3 == 0 || i % 4 == 0){
            console.log(i);
        }
    }
}

printDivisible(-1);
printDivisible('4');
printDivisible(4);
printDivisible(20);