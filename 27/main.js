function guessingGame(){
    // prep the game
    // set the range
    // set the number of guesses
    // choose an answer from the given range at random
    
    var range = insertRange();
    var numberOfGuesses = parseInt(prompt("insert maxNumber of Guesses,for infinite tries : press enter",-1));
    var answer = Math.floor(Math.random() * (range[1] - range[0] +1) + range[0]);
    
    //start the guessing game
    guess(range,numberOfGuesses,answer);
}

function insertRange(){
    //returns two element array containing bottom and top range of the number;
    var result = new Array(2);
    result[0] = parseInt(prompt("insert the bottom range",0));
    result[1] = parseInt(prompt("insert the top range",10));
    if( isNaN(result[0]) || isNaN(result[1])){
        console.log("one of the valuus provided was not a number");
        return null;
    }
    return result;
}

function guess(range,numberOfGuesses,answer){
    //main loop of the game
    //ask the user for the answer until he reaches the max number of answers, he guesses the answer
    // or he asks to quit
    var tries = 1;
    var guess = false;
    while(tries <= numberOfGuesses || numberOfGuesses === -1){
        guess = userInput(answer);
        if(guess === 1){
            //user guessed the answer
            console.log("Congratulations! user guessed the answer: "+answer +", after: " + tries + " tries. Well done!");
            return;
        }
        else if(guess === 0){
            //user failed to guess the answer
            tries++;    
        }
        else if (guess === -1){
            //user requested stopping the game
            console.log("User requested stopping the game after: " + tries + "tries. Come back soon!");
            return;
        }
    }
     console.log("User failed to guess the answer after: " + tries + "tries. Try again!");
}

function userInput(answer){
    //take the user input and compare it to the answer
    //return 1 if the answer is correct
    // return 0 if the naswer is false
    // return -1 if the user wants to stop playing
    var input = parseInt(prompt("guess, input '-1' if you want to stop the game",0));
    if(answer === input){
        return 1;
    }
    else if(input === -1){
        return -1;
    }
    return 0;
}

function showAlertBox(message){
    //helper function, used to display an alert box with the given message
    $('#alert-box').html("<div class='alert alert-danger fade in'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><span id='alert-message'><strong>Error!</strong> "+message+"</span></div>");
}
function checkSettings(rangeFrom,rangeTo,numOfTries){
    //checks wether the settings are of correct type and correct value
    //show alert boxes with errors
    if(isNaN(rangeFrom) || isNaN(rangeTo) || isNaN(numOfTries)){
        //display flash message
        showAlertBox("Wrong type of input inserted.")
        return false;
    }
    if(rangeFrom < 0 || rangeFrom > rangeTo){
        //display flash message
        //console.log("wrong input inserted");
        showAlertBox("Wrong value of input inserted.")
        return false;
    }
    return true;
}

function showEndgameControls(win,message){
    //hide game controls
    //display end message
    //enable show restarting button/message
    if(win){
        $('#header >h1').text("Congratulations");
        $('#header >h1').css('color','green');
    }
    else{
        $('#header >h1').text("Sorry!");
    }
    $('#header >h2').text(message);
    var controls = $('#controls');
    var endgame_controls = $('#endgame-controls');
    controls.toggle();
    endgame_controls.toggle();
}

$(document).ready(function(){
    var numOfTries = "";
    var rangeTo = "";
    var rangeFrom = "";
    var answer = "";
   
    $('#setup').click(function(){
        var settings = $('#settings');
        var controls = $('#controls');
               
        rangeFrom = $('#rangeFrom').val();
        rangeTo = $('#rangeTo').val();
        numOfTries = $('#tries').val();
        
        rangeFrom = parseInt(rangeFrom);
        rangeTo = parseInt(rangeTo);
        numOfTries = parseInt(numOfTries);
        
        if(!checkSettings(rangeFrom,rangeTo,numOfTries)){
            return null;
        }
        
        if(numOfTries <0){
            $("#counter").html('&infin;');    
        }else{
            $("#counter").text(numOfTries);    
        }
        settings.toggle();
        controls.toggle();
        answer = Math.floor(Math.random() * (rangeTo - rangeFrom +1) + rangeFrom);
      });
    
    $('#guess').click(function(){
        console.log("click");    
        var userAnswer = parseInt($('#userInput').val());
        numOfTries--;
        if(userAnswer === -1){
            //user requested quit
           showEndgameControls(false,"You've chosen to quit");
        }
        else if(userAnswer > rangeTo){
            showAlertBox("The provided answer is greater than the starting range");
        }
        else if(userAnswer< rangeFrom){
            showAlertBox("The provided answer is less than the starting range");
        }
        else if(userAnswer === answer){
            //user guessed the correct answer
            showEndgameControls(true, "You've guessed the correct answer");
        }
        else if(numOfTries == 0){
            //user ran out of tries
            showEndgameControls(false,"sorry, you've ran out of tries");
        }
        else{
            $('#header > h2').text("Guess again");
        }
        if(numOfTries <0){
            $("#counter").html('&infin;');    
        }else{
            $("#counter").text(numOfTries);    
        }
        
        console.log(answer);
    });
    $('#restart').click(function(){
        location.reload();
    });
});

