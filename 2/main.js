function myGetDay(numberOfTheDay){
    //accepts a number
    //returns a string with a day of the week
    switch(numberOfTheDay){
        case 1:
            return "monday"
        case 2:
            return "tuesday"
        case 3:
            return "wednesday"
        case 4:
            return "thursday"
        case 5:
            return "friday"
        case 6:
            return "saturday"
        case 7:
            return "sunday"
        default:
            return "wrong input argument"
    }
}

console.log(myGetDay(2));
console.log(myGetDay(-2));
console.log(myGetDay(10));