function myEuclid(arg1,arg2){
    //division based implementation of euclid's algorithm
    //sorks with negative numbers
    while(arg2 !== 0){
        var temp = arg2;
        arg2 = arg1 % arg2;
        arg1 = temp;
    }
    return arg1;
}


console.log(myEuclid(10,5));
console.log(myEuclid(49,28));
console.log(myEuclid(49,-28));
