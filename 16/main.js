function checkOccurance(list_1, list_2){
    if(typeof(list_1) !== 'object' || typeof(list_2) !== 'object'){
        console.log('one of arguments is not a list');
        return -1;
    }
    var sameFound = false;
    for(var i = 0; i<list_1.length;i++){
        for(var k = 0; k<list_2.length;k++){
            if(list_1[i] === list_2[k]){
                return true;
            }
        }
    }
    return false;
}

console.log(checkOccurance([1,2,3,4,5,6],[7,8,9,10,11,2]));
console.log(checkOccurance(["mon","tue","wed","thur","fri","sat","sun"],
                          ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]));
console.log(checkOccurance([1,2,3],
                          ['1','2','3']));
            
console.log(checkOccurance([1,2,3],
                          ['1','2',3]));
            