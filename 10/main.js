function timeFromSpeed(speed){
    //function exptects speed to be an integer value representing speed in km/h
    var distance = 132;
    var result = 0;
    if(typeof(speed) != 'number' || speed <= 0){
        console.log("error. Speed: " + speed + " is not a number or it's value is le 0");
        return -1;
    }
    result = distance/speed;
    return convertTime(result);
}

function convertTime(number){
    var decimal = number % 1;
    var minutes = 60 * decimal;
    return "time is " + (number | 0)+ " hours " + minutes.toFixed(0) + " minutes";
}

console.log(timeFromSpeed("negative value"));
console.log(timeFromSpeed(-1));
console.log(timeFromSpeed(0));
console.log(timeFromSpeed(100));
console.log(timeFromSpeed(150));

