function evenOdd(arg1,arg2){
    //accepts two numbers, returns a series containing even numbers ascending
    // then odd numbers descending in range of those numbers in an array
    var smaller;
    var greater;
    var result = new Array();
    if(arg1 > arg2){
        //check which is greater
        smaller = arg2;
        greater = arg1;
    }else if( arg1 == arg2){
        //if the numbers are even return just this one number
        result.push(arg1);
        return result;
    }else{
        smaller = arg1;
        greater = arg2;
    }
    if( smaller % 2 == 1){
        //the smaller is odd
        tmp = smaller+1;
    }else{
        tmp = smaller;
    }
    while(tmp <= greater){
        result.push(tmp);
        tmp = tmp + 2;
    }
    if(greater % 2 == 0){
        //the greater is even
        tmp = greater - 1;
    }
    else{
        tmp = greater;
    }
    while(tmp >= smaller){
        result.push(tmp);
        tmp = tmp -2;
    }
    return result;
}


console.log(evenOdd(3,8));
console.log(evenOdd(8,3));
console.log(evenOdd(-10,3));