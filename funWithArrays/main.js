function fillArray(){
    var myArray = [];
    for(var i = 1; i<=10; i++){
        myArray[i] = 101 + i;
    }
    return myArray;
}

function fillArray2(){
    var myArray = [];
    for(var i = 1; i<=10; i++){
        myArray[i] = 10 * i;
    }
    return myArray;
}

function fillArray3(){
    var myArray = [];
    for(var i = 1; i<=20; i++){
        if(i%2 == 1){
            myArray[i] = false;    
        }else{
            myArray[i] = true;
        }
        
    }
    return myArray;
}

function fillArray4(){
    var myArray = new Array(100);
    for(var i=0;i<myArray.length;i++){
        myArray[i] = i%10;
    }
    return myArray;
}

function reverseArray(){
    //create the array;
    var myArray = new Array(10);
    for(var i=0;i<myArray.length;i++){
        myArray[i] = i;
    }
    console.log(myArray);
    //reverse the array
    // iterate only on half of the array lenght, otherwise the reverse will have no effect
    for(var i=0;i<myArray.length/2;i++){
        var tmp = myArray[i];
        myArray[i] = myArray[myArray.length -1 - i];
        myArray[myArray.length -1 - i] = tmp;
    }
    return myArray;
}

console.log(reverseArray());