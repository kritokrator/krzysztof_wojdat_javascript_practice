function myAbs(arg){
    //an example way of calculating the absolute value
    if(arg < 0){
        return -arg;
    }
    return arg;
}

function myAbsOw(arg){
    //shorthand version of the function
    return (arg < 0) ? -arg : arg;
}

console.log(myAbsOw(-5));