class Ceasar{
    //class emulating Ceasar cypher
    constructor(){
     console.log("constructor");
    }
    
    setOffset(newOffset){
        this.offset = newOffset;
        console.log("setOffset");
    }

    getOffset(){
        return this.offset;
    }

    encrypt(plaintext){
        //encypts lowercase english alphabet plaintext
        console.log("encrypt");
        var cyphertext = this.convertToUnicode(plaintext);
        cyphertext = this.modifyText(cyphertext,this.offset);
        
        cyphertext = this.convertToText(cyphertext);
        return cyphertext;
    }
    
    decrypt(cyphertext){
        console.log("decrypt");
        var plaintext = this.convertToUnicode(cyphertext);
        plaintext = this.modifyText(plaintext,this.offset * -1);
        
        plaintext = this.convertToText(plaintext);
        return plaintext;
    }
 
    modifyText(text,offset){
        //accepts an array of chars converted to unicode and the offset by which to move them
        //returns modified array
        var result = [];
        for(var i =0 ; i<text.length;i++){
            var tmp = text[i] + offset;
            if(tmp > 122){
                // we've passed the accepted value of unicode lowercase latin letters
                var diff = tmp - 123;
                //substract from 123 not 122 so we can simply add thedifference to 97 - unicode for 'a'
                tmp = 97 + diff;
            }else if(tmp < 97){
                var diff = 96 - tmp;
                //substract from 96 not 97 so we can simply substract the difference from 122 - unicode for 'z'
                tmp = 122 - diff;
            }
            result.push(tmp);
        }
        return result;
    }
    convertToText(codedArray){
        //accepts an array of unicode values,
        //returns a string
        var result =[];
        for(var i = 0; i < codedArray.length; i++){
            result.push(String.fromCharCode(codedArray[i]));
        }
        result = result.join("");
        return result;
    }
    convertToUnicode(text){
        //converts the characters to unicode values and returns them in an array
        var myArr = [];
        for(var i =0;i<text.length;i++){
            myArr.push(text.charCodeAt(i));
        }
        return myArr;
    }
   
}


var encryptor = new Ceasar();
encryptor.setOffset(1);

var cypher = encryptor.encrypt("hello");
console.log(cypher);

var decrypted = encryptor.decrypt(cypher);
console.log(decrypted);

encryptor.setOffset(10);
cypher = encryptor.encrypt("zzzzz");
console.log(cypher);

decrypted = encryptor.decrypt(cypher);
console.log(decrypted);
