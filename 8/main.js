function pyramid(level){
    //return the number of blocks of the pyramid
    if(typeof(level) === 'number' && level > 0)
    {
        return (level * (1 + level))/2
    }
    else{
        console.log("wrong input. " + level + " is not a number or the value is less than 1");
        return -1;
    }
        
}

console.log(pyramid(-1));
console.log(pyramid(0));
console.log(pyramid(2));
console.log(pyramid(3));
console.log(pyramid("10"));