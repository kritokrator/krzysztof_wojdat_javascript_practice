$(document).ready(function(){
    var currentSymbol = 'O', usedIndexes = [];
    var winCheck = false;
    function checkForWin(clickData){
        var cs = currentSymbol;
        var possibleWins = [];
        var winConditions = [
            [1,2,3],
            [4,5,6],
            [7,8,9],
            [1,4,7],
            [2,5,8],
            [3,6,9],
            [1,5,9],
            [3,5,7]
        ]   
        for(var arr in winConditions){
            if(winConditions[arr].indexOf(clickData) != -1){
                possibleWins.push(winConditions[arr]);
            }
        }
        
        for(var arr in possibleWins){
            var elem = [];
            var returnArray = [];
            
            elem.push($('[data-pos='+possibleWins[arr][0]+']'));
            elem.push($('[data-pos='+possibleWins[arr][1]+']'));
            elem.push($('[data-pos='+possibleWins[arr][2]+']'));
            console.log(elem);
            console.log(cs);
            if(elem[0].html() == cs &&
               elem[1].html() == cs &&
               elem[2].html() == cs)
            {
                returnArray.push(elem[0]);
                returnArray.push(elem[1]);
                returnArray.push(elem[2]);
                return returnArray;
            }
            
        }
        return returnArray;
    }
        
    function showEndMessage(message){
                $('.modal-body').html(message);
                
                //show the auxillary reset button
                $('.hidden-reset').show();
                
                //remove event listeners for the columns, disable further game
               winCheck = true;
                $('#myModal').modal('show');
    }
    
    function checkGameStatus(clickData){
            var result = checkForWin(clickData);
            if(result.length > 0){
                //we have a win
                result[0].css("background-color","#e6e600");
                result[1].css("background-color","#e6e600");
                result[2].css("background-color","#e6e600");
                
                showEndMessage('Congratulations! The '+ currentSymbol + ' player has won!')
                
            }
        else if(usedIndexes.length == 9){
                showEndMessage('Sorry! There is a tie. No-one has won!');
        }
    
    }
    
    $('.reset').click(function(){
        //reload the page
        location.reload();
    });
    
    $('.col-md-4').click(function(){
        if(winCheck)
            return;
        var clickData = parseInt($(this).data('pos'));
        console.log("click on: " + clickData);
        if(usedIndexes.indexOf(clickData) != -1){
            return;
        }
        $(this).html(currentSymbol);
        usedIndexes.push(clickData);
        
        checkGameStatus(clickData);
        
        if(currentSymbol === 'O'){
            currentSymbol = 'X';
        } else{
            currentSymbol = 'O';
        }
        
        
    });
});