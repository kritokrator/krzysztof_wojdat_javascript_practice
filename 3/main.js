function myGetMonthName(arg){
    // 0 indexed
    //accepts a number of the month returns a string with it's name
    var months = ["january","february","march","april","may",
                  "june","july","august","september","october",
                  "november","december"];
    
    if(arg >= 0 && arg < months.length){
        return months[arg];
    }
    else{
        return "wrong argument value";
    }
}

console.log(myGetMonthName(0));
console.log(myGetMonthName(-2));
console.log(myGetMonthName(10));
console.log(myGetMonthName(12));
console.log(myGetMonthName(13));