function squaresSum(arg){
    //return the sum of the squares from the range 0 - arg
    result = 0;
    if(arg < 0 || arg >10){
        //number not in range
        console.log("number: " + arg +" not in range 0 - 10");
        return -1;
    }else{
        var base = 0;
        while(base <= arg){
            result += Math.pow(base,2);
            base +=1;
        }
    }
    return result;
}

console.log(squaresSum(-1));
console.log(squaresSum(0));
console.log(squaresSum(4));
console.log(squaresSum(10));
console.log(squaresSum(12));