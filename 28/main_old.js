function Hangman(){
    var dictionary = [
        "hello",
        "world",
        "foobar"
    ];
    
    var answerIndex = Math.floor(Math.random() * (dictionary.length));
    mainLoop(dictionary,answerIndex);
}

function mainLoop(dictionary,answerIndex){
    //the main loop of the game checks the input of the player
    //if the input is single character checks to see if it in the word
    //if the input is a string it checks if the player guessed the entire word
    //on input '-1' game quits
    var maskedWord = maskWord(dictionary[answerIndex]);
    console.log(maskedWord);
    while(true){
        var input = prompt("Guess the next letter!","");
        if(input == '-1'){
            console.log("See you next time!");
            break;
        }
        else if(input.length >1){
            if(input === dictionary[answerIndex]){
                console.log("Wow you've guessed the entire word! The answer: " + input + " Congratulations!");
                break;
            }
        }
        else{
            maskedWord = unmaskWord(dictionary[answerIndex],maskedWord,input);
            console.log(maskedWord);
            if(checkAnswer(maskedWord)){
                   console.log("Congratulations, you've guessed the word! The answer: " + input);
                    break;
            }
        }
    }
    console.log("Goodbye");
}

function maskWord(word){
    //this function masks the answer
    var result = "";
    for(var i = 0;i<word.length;i++){
        result += "_";
    }
    return result;
}

function checkAnswer(maskedWord){
    //checks if all of the passphrase has been guessed
    for(var i=0;i<maskedWord.length;i++){
        if(maskedWord[i] === '_'){
            return false;
        }
    }
    return true;
}
function unmaskWord(word,maskedWord,character){
    //reveals all occurances of the given character in the passphrase
    var tmp_maskedWord = maskedWord.split('');
    for(var i = 0;i<word.length;i++){
        if(word[i] === character){
            tmp_maskedWord[i] = character;
        }   
    }
    maskedWord = tmp_maskedWord.join("");
    return maskedWord;
}

console.log(maskWord("hello"));
console.log(unmaskWord("hello","_____","h"));
console.log(checkAnswer("___"));
console.log(checkAnswer("hello"));

Hangman();
