function randomWord(dictionary){
    //returns a random word from the provided dictionary    
    var index = (Math.random()*dictionary.length) | 0;
    console.log(index)
    return dictionary[index];
}

function coverWord(word){
    return "_".repeat(word.length);
}

function showLetters(word,letter,indexes){
    //fast function for unmasking
    //specific letters in the passphrase
    //
    var tmpArr = word.split("");
    if(indexes.length >0)
    {
        for(var i in indexes){
            tmpArr[indexes[i]] = letter;
        }
    }
    return tmpArr.join("");
}

function showLostDialog(){
    $('.wordCount').html('Przegrana').css('color','#cc0000');
}

function showWinDialog(){
    $('.wordCount').html('Wygrana').css('color','#99e600');
}

function checkWin(sentence){
    var split = sentence.split('');
    for(var i in sentence){
        if(sentence[i] === '_'){
            return false;
        }
    }
    return true;
}
function countLives(sentence){
    var tmpArr = [], splitWord = sentence.split('');
    for(var i in splitWord){
        if(tmpArr.indexOf(splitWord[i]) == -1){
            tmpArr.push(i);
        }
    }
    return tmpArr.length;
}
$(document).ready(function(){
    var dictionary = ["slowo","oko","konkwistador","helloword"];
    var usedLetters = [];
    
    var word = randomWord(dictionary).toLowerCase();
    console.log(word);
    var coveredWord = coverWord(word);
    var tries = countLives(word);
    var win = false;
    
    $('.wordCount').html(coveredWord);
    $('.choicesLeft').html(tries);
    
    $('#guessWord').click(function(){
        var wordTry = $('#word');
        wordTry = wordTry.val();
        console.log(wordTry);
        if(wordTry === word){
            win = true;
            showWinDialog();
        }
        return false;
    });
    $('.letter').click(function(){
        //console.log((this).value);
        var letter = $(this).val().toLowerCase();
        if(tries == 0 ){
            return;
        }
        if(win == true){
            return;
        }
        tries--;
        console.log(letter);
        var guessedLetters = [];
        for(var i =0 ; i < word.length;i++){
            if(word[i] === letter){
                guessedLetters.push(i);
            }
        }
        console.log(guessedLetters);
        if(guessedLetters.length == 0){
            $(this).attr('disabled','disabled').css({
             "background":'rgb(215, 41, 41)',
             "border-color":"red"
            });    
        }
        else{
            $(this).attr('disabled','disabled').css({
             "background":'#99e600',
             "border-color":"green"
            });    
        }
        coveredWord = showLetters(coveredWord,letter,guessedLetters);
        console.log(coveredWord);
        $('.wordCount').html(coveredWord);
        
        

        usedLetters.push(letter);
        console.log()
        var tmpUsedLetters = usedLetters.join(', ');
        
        $('.usedLetters').html(tmpUsedLetters);
        $('.choicesLeft').html(tries);
        
        if(tries == 0){
            showLostDialog();
        }
        if(checkWin(coveredWord)){
            showWinDialog();
            win = true;
        }
        console.log(tmpUsedLetters);
        
    });
});