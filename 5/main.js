function getSeconds(h,m,s){
    //arguments are integers
    //h - hours, m-minutes,s-seconds
    //returns number of seconds total
    if(typeof(h) !== 'number' || typeof(m) !== 'number'|| typeof(s) !== 'number'){
        console.log("wrong input");
        return -1;
    }
    h = h * 3600;
    m = m * 60;
    return h + m + s;
}


console.log(getSeconds(10,"30",10));