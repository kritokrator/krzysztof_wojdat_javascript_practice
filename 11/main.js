function difference(numbersArray){
    //returns the difference between the biggest and the smallest number
    var min = Math.min.apply(Math,numbersArray);
    var max = Math.max.apply(Math,numbersArray);
    return max - min;
}

console.log(difference([-1,0,2,3]));
console.log(difference([1,-1,0,20,3]));
console.log(difference([1,-1,0,20,3,-10]));
            