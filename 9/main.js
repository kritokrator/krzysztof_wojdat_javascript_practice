function compoundInterest(capital, years,interest = 0.19){
    //calculate the value of compound interest
    //years and captial should be an integer value
    //interst is a floating point value containing the yearly interest
    if(typeof(years) === 'number' || typeof(capital) === 'number')
    {
        var result = capital * Math.pow((1 + interest),years);
        return result;
    }
    console.log('error. Years or capital is not a number');
    return -1;
}

console.log(compoundInterest(1000,1,0.5));