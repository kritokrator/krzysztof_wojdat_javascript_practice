function reverseWords(word){
    //print letters of a word, one by one, in a reverse order
    
    if(typeof(word) !== 'string'){
        console.log('argument : ' + word + 'is not a string');
        return -1;
    }
    for(var i = word.length - 1; i >=0;i--){
        console.log(word[i]);
    }
}

reverseWords('helloWorld');
reverseWords('konstantynopolitanczykowianeczkowna');
