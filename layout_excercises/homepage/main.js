$(document).ready(function(){
    $('#search-box').click(function(e){
        $(this).addClass('search-box-active');
        $('#search-input').addClass('search-input-active');
        $('#search-button').addClass('search-button-active');
        $('#search-input').focus();
        e.stopPropagation();
        
    });
    
    $(document).click(function(){
        $('#search-box').removeClass('search-box-active');
        $('#search-input').removeClass('search-input-active');
        $('#search-button').removeClass('search-button-active');
        $('#search-input').val('');
    });
});