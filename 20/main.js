function makeTableOfContests(){
    //get the list of headers and pass it to the function prepearing the list
    var mainHeaders = document.getElementsByTagName("h1");
    var list = prepList2(mainHeaders);
    document.getElementsByTagName("h1")[0].insertAdjacentHTML('beforeBegin',list);
}


function prepList2(listOfHeaders){
    //itearate over all siblings of the first top level header
    //and add the element at the apropriate level
    var list = "<ul>";
    var node = listOfHeaders[0];
    var counter = 0;
    while(node != null){
        
        if(node.tagName === "H1"){
            //the element is a top level header, add it as a first order list element
            if(node.previousSibling.previousSibling != null  &&
               node.previousSibling.previousSibling.tagName === 'P') {
                //there was a previous text node end the previous level
                list+='</ul>';
            }
            node.setAttribute('id', 'id_'+counter);
            list += "<li><a href=#id_"+counter + ">"+node.textContent+"</a></li>"
            counter +=1;
        }
        if(node.tagName === "H2"){
            if(node.previousSibling.previousSibling.tagName === 'H1'){
                //the previos table of content element was a top level header, start the second level table of contents
                list+='<ul>';
            }
            node.setAttribute('id', 'id_'+counter);
            list += "<li><a href=#id_"+ counter+">"+ node.textContent+"</a></li>"
            counter +=1;
        }
        node = node.nextSibling;
    }
    list += "</ul>";
        
    alert(list);
    return list;   
}

makeTableOfContests();