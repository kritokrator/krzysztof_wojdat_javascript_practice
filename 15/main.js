function longerWords(sentence,length){
    //returns an array containing the words from the sentence longer than the given value
    if(typeof(sentence) !== 'string'){
        console.log('value of the sentecne is ' + sentence +'not a string');
        return -1;
    }
    if(typeof(length) !== 'number'){
        console.log('value of the sentecne is ' + length +'not a number');
        return -1;
    }
    if(length < 0){
        console.log('length of the word is less than 0. It is : ' + length);
        return -1;
    }
    var words = sentence.split(' ');
    var result = new Array();
    for(var i =0;i<words.length;i++){
        if(words[i].length > length){
            result.push(words[i]);
        }
    }
    return result;
    

}

console.log(longerWords("The quick brown fox, jumps over lazy dog",4));
console.log(longerWords("I'd rather have a bottle in front of me than a frontal lobotomy",5));