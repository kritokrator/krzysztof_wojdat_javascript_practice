$(document).ready(function(){
    var playingCards = []
    
    var gamblersCards = [];
    var gamblersHandValue = [];
    
    
    var banksCards = [];
    var banksHandValue = [];
    
    var playerCash = 200;
    var currentBet = 0;
    
    
    function makeDeck(){
        //function for making a deck
        var deck = [];
        var colors = ['♣','♥','♦','♠'],
            figures = ['A','K','D','J'];
        
        for(var i = 2; i <= 10;i++){
            figures.push(i.toString());
        }
        
        for(var i = 0; i < figures.length;i++){
            for(var k=0;k<4;k++){
                deck.push(figures[i] + ' ' + colors[k]);
            }
        }
        return deck;   
    };
    
    function drawCard(){
        //draw a card and performe neccessary operations if the deck is empty
        if(playingCards.length > 0)
            return playingCards.pop();
        else{
            console.log("no more cards");
            var newDeck = makeDeck();
            playingCards = newDeck;
            return playingCards.pop();
        }
        
    }
    
    function showModal(player,win){
        var modalTitle = $('.modal-title');
        var modalBody = $('.modal-body');
        if(win){
            modalTitle.html("Congratulations!");
            modalBody.html(player + "has won");
        }else{
            modalTitle.html("Sorry!");
            modalBody.html(player + "has lost");
        }
        
        if(playerCash <= 0){
            modalTitle.html("Sorry!");
            modalBody.html("You've lost all your money.<br/> You can't play again");
            $('#dealNewHand').toggle();
            $('.container').addClass('overlay');
            displayCash();
            displayCurrentBet();    
        }
        
        $('#myModal').modal('show');
    }
    
    function endGame(player,winOrLoss){
        //very Q&D correct tommorow for a nice modal
        if(winOrLoss){
            //the player has won
            if(player == "gambler"){
                playerCash += currentBet;
            }
            if(player == "bank"){
                playerCash -= currentBet;
                
            }
            console.log(player + " has won");
        }
        else{
            //the player has lost
            console.log(player + " has lost");
            if(player == "gambler"){
                playerCash -= currentBet;
            }
            if(player == "bank"){
                playerCash += currentBet;
                
            }
        }
        currentBet = 0
        showModal(player,winOrLoss);
    }
    
    function compareHandValues(){
        //sort the values so we can easily acces the bigger one
        //though honestly only the second one should be larger since that is the order in which we add the aces values
        gamblersHandValue = gamblersHandValue.sort(function(a,b){return a - b;});
        banksHandValue = banksHandValue.sort(function(a,b){return a - b;});
        
        var gamblersHighest = gamblersHandValue[gamblersHandValue.length - 1];
        var banksHighest = banksHandValue[banksHandValue.length -1];
        
        if( gamblersHighest > banksHighest){
            //the gambler has won
            endGame("gambler", true);
        }
        else if (banksHighest > gamblersHighest){
            //the bank has won
            endGame("bank", true);
        }
        else{
            // no-one has won
            endGame("everbody",false);
        }
        
    }
    
    function checkforWin(currentPlayersHand,currentPlayer,currentPlayersHandValue){
        //this is more of a check for a loss function call this function after each hand has been drawn
        //to see if the player crossed the 21 threshold or perhapse drew to aces at the start of the game
        
        //special check for a two ace situation
        
        if(currentPlayersHand.length == 2){
            var firstCardValue = currentPlayersHand[0].split(' ')[0];    
            var secondCardValue = currentPlayersHand[0].split(' ')[0];
            if(firstCardValue == 'A' && secondCardValue == 'A'){
                //current player has won
                //launch the function for handling win situation
                endGame(currentPlayer,true);
                return true;
            }        
        }else{
            //check if all of the possible hand values are above 21 if they are remove them
            for(var i = currentPlayersHandValue.length-1; i >= 0;i--){
                if(currentPlayersHandValue[i] > 21){
                    //we iterate backwards through all (both of them) values in the currentPlayersHand and if they are over 21 we remove them
                    currentPlayersHandValue.pop();
                }
            }
        }
        if(currentPlayersHandValue.length == 0){
            //this player has lost
            //launch the function for handling loss situation
            endGame(currentPlayer,false);
            return true;
        }
        
        return false;
    }
    
    function getCardValue(card){
        //helper function for getting the value of the card
        var result = [];
        var cardValue = card.split(' ')[0];
        if(cardValue == 'A'){
            //this is an ace so it has two possible values
            result.push(1);
            result.push(11);
        }
        else if(['K','D','J'].indexOf(cardValue) != -1){
            //we have a figure so the value is 10
            result.push(10);
        }
        else{
            //this is an ordinary card so we just add parse it's value
            result.push(parseInt(cardValue));
        }
        return result;
    }
    
    function countCardsValue(newCard, currentHandValue){
        //function for counting the value of cards currenty in hand
        //returns an array containing possible hand values (depending on any aces in hand)        
        console.log(newCard);
        var cardValue = getCardValue(newCard);
        if(currentHandValue.length == 0){
            //this is the card added to the hand
            for(var i in cardValue){
                currentHandValue.push(cardValue[i]);
            }
        }
        else if(currentHandValue.length == 2 && cardValue.length == 2){
            //this s a second ace the player has seen
            //however not in direct sequence, that special case should be handled in the checkForWin functin
            currentHandValue[0]++;
            currentHandValue[1]++;
        }
        else if(cardValue.length == 2 && currentHandValue.length == 1){
            currentHandValue.push(currentHandValue[0]);
            currentHandValue[0] += cardValue[0];
            currentHandValue[1] += cardValue[1];
        }
        else if(cardValue.length == 1 && currentHandValue.length == 2){
            currentHandValue[0] += cardValue[0];
            currentHandValue[1] += cardValue[0];
        }
        else{
            currentHandValue[0] += cardValue[0];
        }
            
        console.log(currentHandValue);
    }
    
    function shuffle(arr){
        //function for shuffling cards
        
        var rand, temp;
        for(var i = arr.length; i > 0;i--){
        rand = (Math.random() * i) | 0;
            temp = arr[i-1];
            arr[i-1] = arr[rand];
            arr[rand] = temp;
        }
        return arr;                                     
    };
    
    function displayHands(){
        
        //display the current hands of the players
        var bankHand = $('#bankHand');
        var playerHand = $('#playerHand');
        //TODO this should be fixed to adding only another drawn card
        
        bankHand.html("");
        playerHand.html("");
        
        for(var card in banksCards){
            bankHand.html(bankHand.html() + banksCards[card] + "<br/>");
        }
        for(var card in gamblersCards){
            playerHand.html(playerHand.html() + gamblersCards[card] + "<br/>");
        }
        
    }
    
    function displayTotals(){
        //function for displaying the current score of both players
        var bankTotal = $('#bankTotal');
        var playerTotal = $('#playerTotal');
        
        if(banksHandValue.length == 2){
            bankTotal.html(banksHandValue[0] + "/" + banksHandValue[1])    
        }
        else{
            bankTotal.html(banksHandValue[0]);
        }
        
        if(gamblersHandValue.length == 2){
            playerTotal.html(gamblersHandValue[0] + "/" + gamblersHandValue[1])    
        }else{
            playerTotal.html(gamblersHandValue[0]);
        }
        
    }
    
    function displayCash(){
        $('#playerCashAmount').html(playerCash);
    }
    
    function displayCurrentBet(){
        $('#currentBetAmount').html(currentBet);
    }
    
    $('#playerDraw').click(function(){
        if(currentBet == 0){
            alert("you need to place a bet to play");
            return;
        }
        var newlyDrawnCard = drawCard();
        gamblersCards.push(newlyDrawnCard);
        countCardsValue(newlyDrawnCard,gamblersHandValue);
        
        displayHands();
        displayTotals();
        
        checkforWin(gamblersCards,"gambler",gamblersHandValue);
    });
    
    function conservativeBank(){
        //logic for the bank
        var check = false;
        while(true){
            
            var randomValue = (Math.random() * 100 )| 0;
            banksHandValue = banksHandValue.sort(function(a,b){return a - b;});
            var chanceNotToDraw = (banksHandValue[banksHandValue.length -1] / 21) * 100;
            if(randomValue < chanceNotToDraw)
            {
                break;
            }
            else{
                var newlyDrawnCard = drawCard();
                banksCards.push(newlyDrawnCard);
                countCardsValue(newlyDrawnCard,banksHandValue);
        
                
                displayHands();
                displayTotals();
                
                checkforWin(banksCards,"bank",banksHandValue);
                
            }      
            $(this).delay(1000);
        }
        //the bank has decided not to draw
        compareHandValues();
    }
    
    
    
    function adventurousBank(){
        //logic for the bank
        while(true){
            
            var randomValue = (Math.random() * 100 )| 0;
            banksHandValue = banksHandValue.sort(function(a,b){return a - b;});
            var chanceToDraw = ((21 - banksHandValue[banksHandValue.length -1]) / 11) * 100;
            
            var check = false;
            if(randomValue > chanceToDraw)
            {
                check = true;
                break;
            }
            else{
                var newlyDrawnCard = drawCard();
                banksCards.push(newlyDrawnCard);
                countCardsValue(newlyDrawnCard,banksHandValue);
        
                displayHands();
                displayTotals();
        
                if(checkforWin(banksCards,"bank",banksHandValue)){
                    //there was a win
                    break;
                }
            }      
        }
        if(check){
            //the bank has decided not to draw
            compareHandValues();
        }
    }
    
    
    $('#playerCheck').click(function(){
        //conservativeBank();     
        adventurousBank();
    });
    
    $('#playerPass').click(function(){
        if(currentBet != 0){
            alert('cannot pas when a bet has benn made');
        }
        dealNewHand();
    });
    
    $('#dealNewHand').click(function(){
        dealNewHand();
    });
    
    $('#placeBet').click(function(){
        currentBet = parseInt($('#betAmount').val());
        
        if(isNaN(currentBet)) {
            alert('place a correct value for a bet');
            return;
        }
        if(currentBet > playerCash){
           alert("cannot place bet greater than the amount of current cash");
           return;
        }
        $("#placeBet").prop("disabled", (_, val) => !val);
        
        alert(currentBet);
        displayCurrentBet();
    });
    
    
    $('.overlay').click(function(){
        return;
    });
    
    
    
    
    
    
    function dealNewHand(){
        //this function deals another hand for the players;
        gamblersCards = [];
        gamblersHandValue = [];
        
        banksCards = [];
        banksHandValue = [];
        
            
        gamblersCards.push(drawCard());
        countCardsValue(gamblersCards[0],gamblersHandValue);
        
        banksCards.push(drawCard());
        countCardsValue(banksCards[0],banksHandValue); 
        
        gamblersCards.push(drawCard());
        countCardsValue(gamblersCards[1],gamblersHandValue);
        
    
        
        $("#placeBet").prop("disabled", (_, val) => false);
        
        displayHands();
        displayTotals();
        displayCash();
        displayCurrentBet();
        
    }
    
    playingCards = makeDeck();
    playingCards = shuffle(playingCards);
    
    dealNewHand();
    displayCash();
    
    
});