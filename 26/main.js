function ONPCalculator(input){
    //input should be a string of characters containing an expression in reverse polish notation format
    var stack = [];
    var numberHelper=[];
    
    if(typeof(input) !== 'string'){
        console.log("ERROR::input is not a string!");
        return null;
    }
    for(var i =0; i< input.length;i++){
        if(input[i] === '+'){
            var operand2 = stack.pop();
            var operand1 = stack.pop();
            stack.push(operand1 + operand2)
        }
        else if(input[i] === '-'){
            var operand2 = stack.pop();
            var operand1 = stack.pop();
            stack.push(operand1 - operand2)
        }
        else if(input[i] === '*'){
            var operand2 = stack.pop();
            var operand1 = stack.pop();
            stack.push(operand1 * operand2)
        }
        else if(input[i] === '/'){
            var operand2 = stack.pop();
            var operand1 = stack.pop();
            stack.push(operand1 / operand2)
        }
        else if(input[i] === ' '){
            if(numberHelper.length > 0){
                var tmp = numberHelper.join("");
                tmp = parseInt(tmp);
                stack.push(tmp);
                numberHelper = [];
            }
        }
        else{
            numberHelper.push(input[i]);
        }
    }
    return stack.pop();
}

console.log(ONPCalculator("12 2 3 4 * 10 5 / + * +"));
