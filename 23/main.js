function checkPesel(input){
    //checks if the PESEL, provided as a string of characters, is a valid number
    //if so returns the birthdate and the sex of the person 
    if(checkCorrectnes(input)){
        var date = getBirthdate(input);
        var sex = getSex(input.charAt(9));
        if(sex === 1){
            return date + " female";
        }
        else if(sex === 0){
            return date + " male";
        }else if(sex === -1){
            console.log("an error occured while checking the sex of the person");
            return -1;
        }
    
    }else{
        console.log("the number is not a valid pesel number");
        return -1;
    }   
}
function checkCorrectnes(input){
    //accepts the pesel number as a string of characters,
    //returns true if the string given is a valid Pesel number
    //returns false otherwise
    var sum = 0;
    
    sum += 1 * parseInt(input.charAt(0));
    sum += 3 * parseInt(input.charAt(1));
    sum += 7 * parseInt(input.charAt(2));
    sum += 9 * parseInt(input.charAt(3));
    sum += 1 * parseInt(input.charAt(4));
    sum += 3 * parseInt(input.charAt(5));
    sum += 7 * parseInt(input.charAt(6));
    sum += 9 * parseInt(input.charAt(7));
    sum += 1 * parseInt(input.charAt(8));
    sum += 3 * parseInt(input.charAt(9));
    var remainder = sum % 10;
     if(remainder === 0){
        if(input.charAt(10) === '0')
        {
            return true;
        }
        return false
    }
    else{
        var check = 10 - remainder;
        if(check === parseInt(input.charAt(10)))
        {
            return true;
        }
        return false;
    }
    
}
function getBirthdate(input){
    //parsing function, accepts pesel number as a string of characters, returns a string containing the birth date
    return input.charAt(4) + input.charAt(5)+
                       "-" + input.charAt(2) + input.charAt(3) + "-" + input.charAt(0) + input.charAt(1);
}

function getSex(singleCharacter){
    //accepts a single character in range 0 - 9
    //returns 0 if the character was found in the 'male' array
    //returns 1 if the character was found in the 'female' array
    // return -1 if the character was not found and tehrfore an error occured
    var femaleNumbers = [0,2,4,6,8];
    var maleNumbers = [1,3,5,7,9];
    
    var test = parseInt(singleCharacter);
    for(var i=0;i<maleNumbers.length;i++){
        if(maleNumbers[i] === test){
            return 0;
        }
    }
    for(var i=0;i<femaleNumbers.length;i++){
        if(femaleNumbers[i] === test){
            return 1;
        }
    }
    return -1;
}

console.log(checkPesel('88092715354'));