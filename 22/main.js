function isPalindrome(word){
    //check if the given string is a palindrome
    if(typeof(word) !== 'string'){
        console.log('the given argument is not a string');
        return false;
    }
    var tmp_word = word.split('');
    
    tmp_word = tmp_word.reverse();
    for(var i = 0; i < word.length;i++){
        if(word.charAt(i) !== tmp_word[i]){
            return false;
        }
    }
    return true;
}

function isPalindrome2(word){
    if(typeof(word) !== 'string'){
        console.log('the given argument is not a string');
        return false;
    }
    for(var i=0;i<word.length/2;i++){
        if(word[i] !== word[word.length -1 - i]){
            return false;
        }
        
    }
    return true;
}


console.log(isPalindrome2('okop'));